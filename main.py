import controllers.controller as controller

seidel = controller.Seidel()

print('--------------------------------------------------------------------')
print('-------------------------DATOS-GENERALES----------------------------')
print('-------------------------Analisis de Sistemas-----------------------')
print('-------------------Angel Steven Martinez Chamba---------------------')
print('------------------------------Octavo "B"----------------------------')
print('--------------------------------------------------------------------')

# Matriz ampliada
print('-------------------------MATRIZ AMPLIADA----------------------------')
matriz_ampliada = [[10, -1, 2, 6], [1, -5, 1, -10], [2, -1, 8, -8]]
seidel.print_matriz(matriz_ampliada)

# Convertir matriz extendida en forma Ax
print('-------------------------MATRIZ AX----------------------------------')
matriz_ax = seidel.matriz_ax(matriz_ampliada)
seidel.print_matriz(matriz_ax)

# Convertir matriz extendida en forma B
print('-------------------------VECTOR B-----------------------------------')
vector_b = seidel.vector_b(matriz_ampliada)
print(vector_b)

# Obtener la Diagonal Principal
print('-------------------------DIAGONAL-----------------------------------')
diagonal = seidel.matriz_diagonal(matriz_ax)
seidel.print_matriz(diagonal)

# Obtener matriz Lower
print('-------------------------LOWER--------------------------------------')
lower = seidel.matriz_lower(matriz_ax)
seidel.print_matriz(lower)

# Obtener matriz Upper
print('-------------------------UPPER--------------------------------------')
upper = seidel.matriz_upper(matriz_ax)
seidel.print_matriz(upper)

# Sumar la matriz (D+L)
sum_dl = seidel.suma_matriz(diagonal, lower)
print('-------------------------MATRIZ (D+L)-------------------------------')
seidel.print_matriz(sum_dl)

# Obtener la matriz inversa de (D+L)
print('-------------------------INVERSA (D+L)------------------------------')
inversa_dl = seidel.matriz_inversa(sum_dl)
seidel.print_matriz(inversa_dl)

# Multiplicar la matriz inversa (D+L) * matriz Upper
print('-------------------------INV (D+L)*UPPER----------------------------')
dl_upper = seidel.mul_matriz(inversa_dl, upper)
seidel.print_matriz(dl_upper)

print('-------------------------ITERACION-1--------------------------------')
iteracion_1 = seidel.mul_vector(dl_upper, [1, 2, 3])
vector_r1 = seidel.parse_array(iteracion_1)
print(vector_r1)

print('-------------------------ITERACION-2--------------------------------')
iteracion_2 = seidel.mul_vector(dl_upper, vector_r1)
vector_r2 = seidel.parse_array(iteracion_2)
print(vector_r2)

print('--------------------------"-(D+L)-----------------------------------')
n_array = seidel.parse_negative_array(vector_r1)
print(n_array)

print('-------------------------RESULTADO----------------------------------')
sum_iteractions = seidel.sum_vector(n_array, vector_r2)
print(sum_iteractions)
