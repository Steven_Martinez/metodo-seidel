import numpy as np


class Seidel:

    def scan_integer(self, content):
        """
        Metodo de entrada de datos por consola, enteros
        """
        while True:
            entrada = input(content)
            try:
                print(
                    '-------------------------------------------------------------------')
                return (int)(entrada)
            except:
                print("La entrada es incorrecta, escribe un numero entero")

    def enter_equation(self, num_equations: int, num_variables: int):
        """
        Metodo registra los valores de Ax = b, en ka matriz ampliada
        """
        seidel = Seidel
        equation = []
        print('-------------------------------------------------------------------')
        i = 1
        while i <= num_equations:
            print("-----------------------INGRESE LA ECUACION",
                  i, "----------------------")
            j = 0
            aux = []
            while j <= num_variables:
                if j == num_variables:
                    content = "Ingrese el valor de b: "
                    aux.append(seidel.scan_integer(0, content=content))
                else:
                    content = "Ingrese el valor de x" + str(j) + ": "
                    aux.append(seidel.scan_integer(0, content=content))
                j += 1
                content = "Ingrese el valor de x" + str(j) + ": "
            i += 1
            equation.append(aux)
            print('-------------------------------------------------------------------')
        return equation

    def matriz_ax(self, matriz):
        """
        Metodo toma la matriz ampliada y retorna la matriz en forma Ax
        """
        ax = []
        for i in range(len(matriz)):
            aux = []
            for j in range(len(matriz[i][:-1])):
                aux.append(matriz[i][j])
            ax.append(aux)
        return ax

    def vector_b(self, matriz):
        """
        Metodo toma la matriz ampliada y retorna el vector en forma b
        """
        b = []
        for i in range(len(matriz)):
            b.append(matriz[i][-1])
        return b

    def matriz_diagonal(self, matriz):
        """
        Metodo toma la matriz en forma Ax, y retorna la matriz diagonal principal
        """
        diagonal = []
        for i in range(len(matriz)):
            aux = []
            for j in range(len(matriz[i])):
                if i == j:
                    aux.append(matriz[i][j])
                else:
                    aux.append(0)
            diagonal.append(aux)
        return diagonal

    def matriz_lower(self, matriz):
        """
        Metodo toma la matriz en forma Ax, y retorna la matriz lower
        """
        lower = []
        for i in range(len(matriz)):
            aux = []
            for j in range(len(matriz[i])):
                if i > j:
                    aux.append(matriz[i][j])
                else:
                    aux.append(0)
            lower.append(aux)
        return lower

    def matriz_upper(self, matriz):
        """
        Metodo toma la matriz en forma Ax, y retorna la matriz upper
        """
        upper = []
        for i in range(len(matriz)):
            aux = []
            for j in range(len(matriz[i])):
                if i < j:
                    aux.append(matriz[i][j])
                else:
                    aux.append(0)
            upper.append(aux)
        return upper

    def suma_matriz(self, diagonal, lower):
        """
        Metodo suma dos matriz diagonal x matriz lower, retorna la matriz resultante
        """
        matriz_dl = []
        for i in range(len(diagonal)):
            aux = []
            for j in range(len(diagonal[i])):
                aux.append(int(diagonal[i][j]) + int(lower[i][j]))
            matriz_dl.append(aux)
        return matriz_dl

    def matriz_inversa(self, matriz):
        """
        Metodo retorna la matriz inversa
        """
        return np.linalg.inv(matriz)

    def mul_matriz(self, m, n):
        """
        Metodo multiplica matriz MxN
        """
        return np.dot(m, n)

    def mul_vector(self, m, n):
        """
        Metodo multiplica dos vectores resultantes de la iteraciones de Xn+1
        """
        return np.dot(m, n)

    def sum_vector(self, a, b):
        """
        Metodo suma los vectores resultantes de la iteraciones de Xn
        """
        sum = []
        for i in range(len(a)):
            sum.append(float(a[i])+float(b[i]))
        return sum

    def parse_array(self, array):
        """
        Metodo retorna un arreglo de numpy en arreglo convencional, redondeado a 5 digitos
        """
        x = []
        for item in array:
            x.append(float(np.round(item, 5)))
        return x

    def parse_negative_array(self, array):
        """
        Metodo retorna un arreglo de numpy en arreglo convencional, redondeado a 5 digitos
        """
        x = []
        for item in array:
            x.append(-float(np.round(item, 5)))
        return x

    def print_matriz(self, matriz):
        """
        Metodo presenta en pantalla las iteraciones de las diferentes matrices
        """
        for i in range(len(matriz)):
            for j in range(len(matriz[i])):
                print(np.round(matriz[i][j], 5), end=' | ')
            print()
