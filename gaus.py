"""
ANÁLISIS NUMÉRICO (PERIODO ABRIL- SEPTIEMBRE 2021)
                Gauss- Jordán Matrices de (3x3)
Integrantes: 
    - Angel Steven Martinez Chamba
    - Cristian Eduardo Medina Morocho
    - Johanna Patricia Montaño Guaman
"""

import numpy as np


def buildFullMatrix(coefficients: np.matrix, indepTerms: np.array):
    """
    Construir una Matriz Aumentada en forma Ax=b
    """
    rows = len(coefficients)
    cols = len(coefficients[0]) + 1
    matrix = np.zeros((rows, cols))
    for i in range(rows):
        for j in range(cols):
            if j < cols - 1:
                matrix[i][j] = coefficients[i][j]
            else:
                matrix[i][j] = indepTerms[i]
    return matrix


def convertFullMatrixToText(matrix: np.matrix):
    """
        Mostrar la matriz recibida en forma tabulada
    """
    rows = len(matrix)
    cols = len(matrix[0])
    textLine = ""
    output = ""
    for i in range(rows):
        textLine = "   ["
        for j in range(cols):
            item = matrix[i][j]
            if j != cols - 1:
                textLine += "{0:4.2f}".format(item).rjust(10)
            else:
                textLine += "  =" + "{0:4.2f}".format(item).rjust(10)
        textLine += " ]"
        output += textLine + "\n"
    return output.rstrip()


def buildProcessingCoords(matrix: np.matrix):
    """
        Orden por el cual poner a cero las celdas de la matriz
    """
    matrix = np.zeros((6, 2))
    # primero bajo la diagonal principal
    matrix[0] = [3, 1]  # fila 3, col 1
    matrix[1] = [2, 1]  # fila 2, col 1
    matrix[2] = [3, 2]  # fila 3, col 2
    # luego sobre la diagonal principal
    matrix[3] = [1, 3]  # fila 1, col 3
    matrix[4] = [2, 3]  # fila 2, col 3
    matrix[5] = [1, 2]  # fila 1, col 2
    return matrix


def convertProcessingCoordsToText(matrix: np.matrix):
    """
    Mostrar la matriz recibida como una lista de pares de valores
    """
    rows = len(matrix)
    cols = len(matrix[0])
    textLine = "   "
    for i in range(rows):
        textLine += "("
        for j in range(cols):
            textLine += "" if j == 0 else ","
            textLine += "{0:1.0f}".format(matrix[i][j])
        textLine += ") "
    return textLine


def getCellCoordinates(matrix: np.matrix, order: int):
    """
    Coordenadas de la celda a procesar, según el orden recibido
    """
    return int(matrix[order][0]), int(matrix[order][1])


def processRows(targetRow: np.array, otherRow: np.array, indexCol: int):
    """
    Procesar una fila contra otra, llevando a cero la "celda" dada
    """
    targetFactor = targetRow[indexCol]
    otherFactor = otherRow[indexCol]
    return (otherFactor * targetRow) - (targetFactor * otherRow)


def extractIndepTerms(matrix: np.matrix):
    """
    Extraer los valores independientes de la matriz recibida
    """
    rows = len(matrix)
    cols = len(matrix[0])
    output = np.ones(rows)
    for i in range(rows):
        output[i] = matrix[i][cols - 1]
    return output


def main():
    # sistema de ecuaciones lineales Ax=b
    matriz_ax = [[2, 3, -5], [-3, 2, -1], [1, -3, 2]]
    matriz_b = [2, 3, 1]

    coefficients = np.array(matriz_ax)
    indepTerms = np.array(matriz_b)

    print("Resolver el sistema de ecuaciones lineales:")
    fullMatrix = buildFullMatrix(coefficients, indepTerms)
    print(convertFullMatrixToText(fullMatrix), "\n\n")

    print("Orden de las celdas por el cual llevar a cero la matriz (fila, columna):")
    processingOrder = buildProcessingCoords(fullMatrix)
    print(convertProcessingCoordsToText(processingOrder), "\n\n")

    print("Algoritmo de Gauss-Jordan: \n")
    for order in range(len(processingOrder)):
        # la celda a llevar a cero
        indexRow, indexCol = getCellCoordinates(processingOrder, order)
        print("   - procesando celda ({0},{1}):".format(indexRow, indexCol))
        # adaptar indices, ya que en Python estos empiezan desde la posición cero
        indexRow = indexRow - 1
        indexCol = indexCol - 1
        # filas a operar entre sí
        targetRow = fullMatrix[indexRow]
        otherRow = fullMatrix[indexCol]
        # procesar la fila
        newRow = processRows(targetRow, otherRow, indexCol)
        fullMatrix[indexRow] = newRow
        # mostrar avances
        fullMatrixText = convertFullMatrixToText(fullMatrix)
        print(fullMatrixText, "\n")
    print()
    print("Resultados:")
    resultDiagonal = np.diagonal(fullMatrix)
    resultIndepTerms = extractIndepTerms(fullMatrix)
    print(resultIndepTerms / resultDiagonal)


main()
