# Método de Gaus Seidel Matricial

En análisis numérico el método de Gauss-Seidel es un método iterativo utilizado para resolver sistemas de ecuaciones lineales. El método se llama así en honor a los matemáticos alemanes Carl Friedrich Gauss y Philipp Ludwig von Seidel y es similar al método de Jacobi. 

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

Para el correcto funcionamiento del proyecto, requiere la instalacion de las librerias mencionadas en el siguiente documento:

```
- requirements.txt
```

### Instalación 🔧

Se recomienda usar un entorno de desarrollo virtual de python.

_Clonar el repositorio_

```
git clone repositorio_xyz
```

## Despliegue 📦

_Para ejecutar el programa, se debe acceder al archivo main.py para su ejecución

* Python main.py

## Construido con 🛠️

* Python 3.7.3

## Autores ✒️

* **Angel Steven Martinez Chamba** - *Trabajo Inicial*
---
